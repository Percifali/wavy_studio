import { Box, Hide, Show } from "@chakra-ui/react";
import React from "react";
import { ArrowRight } from "../icons";
import useHeader from "../../utils/useHeader";

type Props = {
    onClick: () => void;
};

const ArrowUp: React.FC<Props> = ({ onClick }) => {
    const { shrunk } = useHeader();
    return (
        <Show below="md">
            <Box
                sx={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    position: "absolute",
                    bottom: 4,
                    right: 4,
                    color: "gray.400",
                    transform: `rotate(-90deg) translate(0px, ${
                        shrunk ? 0 : 100
                    }px)`,
                    border: "1px solid",
                    borderColor: "gray.400",
                    borderRadius: "50%",
                    p: 2,
                    cursor: "pointer",
                    transition: "all 0.5s ease-out",
                }}
                onClick={onClick}
            >
                <ArrowRight />
            </Box>
        </Show>
    );
};

export default ArrowUp;
