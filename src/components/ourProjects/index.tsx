import React from "react";
import PageTitle from "../../ui/pageTitle";
import {
    Box,
    BoxProps,
    Button,
    Flex,
    HStack,
    SimpleGrid,
    Tag,
    useMediaQuery,
} from "@chakra-ui/react";
import PageContainer from "../../ui/pageContainer";
import { ArrowRight } from "@components/icons";
import Image from "next/image";
import { useRouter } from "next/router";

const projects = [
    {
        title: "Samsung",
        subtitle:
            "Take a look at how we used React to create a fast and smooth webside builder for a VC-funded Stanford StartX-accelerated startup.",
        image: "project.png",
        tags: ["Research", "Development", "Optimization"],
        color: "red.300",
    },
    {
        title: "Samsung",
        subtitle:
            "Take a look at how we used React to create a fast and smooth webside builder for a VC-funded Stanford StartX-accelerated startup.",
        image: "project.png",
        tags: ["Research", "Development", "Optimization"],
        color: "yellow.300",
    },
    {
        title: "Samsung",
        subtitle:
            "Take a look at how we used React to create a fast and smooth webside builder for a VC-funded Stanford StartX-accelerated startup.",
        image: "project.png",
        tags: ["Research", "Development", "Optimization"],
        color: "blue.300",
    },
    {
        title: "Samsung",
        subtitle:
            "Take a look at how we used React to create a fast and smooth webside builder for a VC-funded Stanford StartX-accelerated startup.",
        image: "project.png",
        tags: ["Research", "Development", "Optimization"],
        color: "purple.300",
    },
];

const OurProjects: React.FC = () => {
    const [isMobile] = useMediaQuery("(max-width: 48rem)");
    const router = useRouter();
    return (
        <PageContainer>
            <PageTitle
                title="Our projects"
                rightComponent={
                    <Button
                        size={isMobile ? "sm" : "md"}
                        variant="black"
                        rightIcon={<ArrowRight />}
                        onClick={() => router.push("/contact")}
                    >
                        Show more
                    </Button>
                }
            />
            <SimpleGrid
                columns={{
                    base: 1,
                    md: 2,
                }}
                spacing={{
                    base: 2,
                    md: 8,
                }}
                mt={{
                    base: 6,
                    md: 4,
                }}
            >
                {projects.map((project, index) => {
                    return <Project key={index} {...project} />;
                })}
            </SimpleGrid>
        </PageContainer>
    );
};

type ProjectProps = {
    title: string;
    subtitle: string;
    image: string;
    color: string;
    tags: string[];
} & BoxProps;

const Project: React.FC<ProjectProps> = ({
    title,
    subtitle,
    image,
    tags,
    color,
    ...props
}) => {
    return (
        <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            sx={{
                cursor: "pointer",
                paddingInline: { base: 2, md: 4 },
                paddingBlock: { base: 3, md: 6 },
                height: {
                    base: "100%",
                    md: "340px",
                },
                border: "1px solid",
                borderColor: "gray.300",
                position: "relative",
                overflow: "hidden",
                transition: "all .3s ease-in-out .1s",
                "&:hover": {
                    backgroundColor: color,
                    "& .subtitle": {
                        transform: "translate(0, 300%)",
                    },
                    "& .title": {
                        transform: {
                            base: "none",
                            md: "translate(0, -50%)",
                        },
                    },
                    "& .img-thumb": {
                        transform: {
                            base: "translate(-50%, 40%)",
                            md: "translate(-50%, 58%)",
                            //lg: "translate(-50%, 60%)",
                        },
                        boxShadow: "0 0 50px rgb(0 0 0 / 30%)",
                        opacity: 1,
                        visibility: "visible",
                    },
                },
            }}
            {...props}
        >
            {/*<Box w="100%" height={250} position="relative">*/}
            {/*    <Image*/}
            {/*        src={`/assets/images/${image}`}*/}
            {/*        layout="fill"*/}
            {/*        objectFit="cover"*/}
            {/*    />*/}
            {/*</Box>*/}
            <HStack
                spacing={{
                    base: 1,
                    md: 3,
                }}
                justifyContent="center"
                sx={{
                    position: "absolute",
                    top: {
                        base: 2,
                        md: 6,
                    },
                    width: "100%",
                }}
            >
                {tags.map((tag) => (
                    <Tag
                        borderRadius="full"
                        px={{
                            base: 2,
                            md: 3,
                        }}
                        backgroundColor="rgba(237,242,247, 0.5)"
                    >
                        {tag}
                    </Tag>
                ))}
            </HStack>
            <Box
                className="title"
                textStyle={{
                    base: "h5",
                    md: "h3",
                }}
                color="black.500"
                transition="all .3s ease-in-out 0s"
                pt={{
                    base: 7,
                    md: 0,
                }}
            >
                {title}
            </Box>
            <Box
                className="subtitle"
                textStyle="body2"
                fontSize={{
                    base: "0.75rem",
                    md: "1.125rem",
                }}
                textAlign="center"
                my={{
                    base: 2,
                    md: 4,
                }}
                maxWidth={{
                    base: "100%",
                    md: "60%",
                }}
                transition="all .3s ease-in-out 0s"
            >
                {subtitle}
            </Box>
            <Box
                className="img-thumb"
                sx={{
                    transition: "all .4s ease 0s",
                    visibility: "hidden",
                    opacity: 0,
                    left: "50%",
                    width: "80%",
                    bottom: 0,
                    maxWidth: 350,
                    position: "absolute",
                    transform: "translate(-50%, 150%)",
                    "& img": {
                        width: "100%",
                        height: "100%",
                    },
                }}
            >
                <img src="https://compire.co/static/media/fw_jumpcut.7d766361.jpg" />
            </Box>
        </Flex>
    );
};

export default OurProjects;
