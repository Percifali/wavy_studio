import * as React from "react";

function SvgTelegram(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            width="1em"
            height="1em"
            viewBox="0 0 28 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <path
                d="M27.92 2.138l-4.225 19.926c-.319 1.407-1.15 1.757-2.332 1.094l-6.438-4.744-3.106 2.988c-.344.344-.632.631-1.294.631l.463-6.557L22.92 4.694c.519-.462-.113-.719-.807-.256l-14.75 9.288-6.351-1.987c-1.382-.432-1.407-1.382.287-2.044L26.14.125c1.15-.431 2.156.256 1.781 2.013z"
                fill="#fff"
            />
        </svg>
    );
}

export default SvgTelegram;
