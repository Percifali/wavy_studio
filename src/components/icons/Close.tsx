import * as React from "react";

function SvgClose(props: React.SVGProps<SVGSVGElement>) {
    return (
        <svg
            width="1em"
            height="1em"
            viewBox="0 0 28 28"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            {...props}
        >
            <path
                d="M14 12.02L25.61.41a1.4 1.4 0 011.98 1.98L15.98 14l11.61 11.61a1.4 1.4 0 01-1.98 1.98L14 15.98 2.39 27.59a1.4 1.4 0 01-1.98-1.98L12.02 14 .41 2.39A1.4 1.4 0 012.39.41L14 12.02z"
                fill="currentColor"
            />
        </svg>
    );
}

export default SvgClose;
