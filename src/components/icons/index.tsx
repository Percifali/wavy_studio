export { default as ArrowRight } from "./ArrowRight";
export { default as Close } from "./Close";
export { default as Email } from "./Email";
export { default as Hamburger } from "./Hamburger";
export { default as Telegram } from "./Telegram";
export { default as Viber } from "./Viber";
export { default as Wavy } from "./Wavy";
export { default as Whatsapp } from "./Whatsapp";
