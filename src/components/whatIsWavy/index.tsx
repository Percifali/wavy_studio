import React, { useRef } from "react";
import PageContainer from "../../ui/pageContainer";
import PageTitle from "../../ui/pageTitle";
import { Box, BoxProps, Flex } from "@chakra-ui/react";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import Slider from "react-slick";
import useMediaQuery from "../../utils/useMediaQuery";
import { ArrowRight } from "@components/icons";

const circles = [
    {
        title: "Digital products",
        desc: "By applying behavioral science to customer experience we design digital products used by millions of people",
    },
    {
        title: "Digital products",
        desc: "By applying behavioral science to customer experience we design digital products used by millions of people",
    },
    {
        title: "Digital products",
        desc: "By applying behavioral science to customer experience we design digital products used by millions of people",
    },
];

const WhatIsWavy: React.FC = () => {
    const isMobile = useMediaQuery("(max-width: 48rem)");
    const ref = useRef<any>(null);

    const settings = {
        dots: true,
        infinite: true,
        arrows: false,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
    };

    return (
        <PageContainer>
            <PageTitle
                title="What is Wavy?"
                alignItems="flex-start"
                flexWrap="wrap"
                rightComponent={
                    <Box
                        maxW={{ base: "100%", md: "50%" }}
                        textStyle="body2"
                        pt={2}
                        textAlign="left"
                    >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Vitae proin sagittis nisl rhoncus mattis
                        rhoncus urna neque viverra. Est ante in nibh mauris
                        cursus mattis molestie.
                    </Box>
                }
            />
            {isMobile ? (
                <>
                    <Slider
                        ref={(slider: any) => (ref.current = slider)}
                        {...settings}
                    >
                        {circles.map(({ title, desc }) => (
                            <div>
                                <Flex
                                    h={400}
                                    w="100%"
                                    justifyContent="center"
                                    alignItems="center"
                                >
                                    <CircleItem
                                        key={title}
                                        title={title}
                                        desc={desc}
                                    />
                                </Flex>
                            </div>
                        ))}
                    </Slider>
                </>
            ) : (
                <Flex
                    flexWrap="wrap"
                    alignItems="center"
                    justifyContent="space-between"
                    my={{
                        base: 8,
                        md: 20,
                    }}
                >
                    {circles.map(({ title, desc }) => (
                        <CircleItem key={title} title={title} desc={desc} />
                    ))}
                </Flex>
            )}
        </PageContainer>
    );
};

type Props = {
    title: string;
    desc: string;
} & BoxProps;

const CircleItem: React.FC<Props> = ({ title, desc, ...props }) => {
    return (
        <Flex
            w={350}
            h={350}
            border="2px solid #111111"
            borderRadius="50%"
            alignItems="center"
            justifyContent="center"
            flexDirection="column"
            color="black.500"
            {...props}
        >
            <Box textStyle="h4" textAlign="center">
                {title}
            </Box>
            <Box textStyle="body2" textAlign="center" maxW="85%" pt={4}>
                {desc}
            </Box>
        </Flex>
    );
};

export default WhatIsWavy;
