import React from "react";
import PageContainer from "../../ui/pageContainer";
import {
    Accordion,
    AccordionButton,
    AccordionIcon,
    AccordionItem,
    AccordionPanel,
    Box,
    Flex,
    SimpleGrid,
} from "@chakra-ui/react";
import SimpleTitle from "../../ui/simpleTitle";
import { ArrowRight } from "@components/icons";
import Head from "next/head";

const items = [
    {
        title: "Digital products",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    },
    {
        title: "Digital products",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    },
    {
        title: "Digital products",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    },
    {
        title: "Digital products",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    },
    {
        title: "Digital products",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    },
];

const WhyWavy: React.FC = () => {
    return (
        <PageContainer>
            <Head>
                <title>Wavy Studio</title>
            </Head>
            <SimpleGrid columns={{ base: 1, md: 2 }}>
                <Flex
                    flexDirection="column"
                    alignItems="flex-start"
                    minH={{ base: "auto", md: 400 }}
                >
                    <SimpleTitle title="Why Wavy?" />
                    <Box
                        textStyle="body2"
                        maxWidth={{
                            base: "100%",
                            md: "50%",
                        }}
                        color="black.500"
                        pt={2}
                    >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua.
                    </Box>
                </Flex>
                <Flex flexDirection="column" alignItems="stretch" my={4}>
                    <Accordion allowToggle>
                        {items.map((item, index) => (
                            <WavyItem key={item.title + index} {...item} />
                        ))}
                    </Accordion>
                </Flex>
            </SimpleGrid>
        </PageContainer>
    );
};

type ItemProps = {
    title: string;
    desc: string;
};

const WavyItem: React.FC<ItemProps> = ({ title, desc }) => {
    return (
        <AccordionItem borderTop="none">
            {({ isExpanded }) => (
                <Box
                    borderBottom="2px solid"
                    borderBottomColor={isExpanded ? "purple.300" : "#111111"}
                >
                    <AccordionButton
                        _expanded={{ color: "purple.300" }}
                        _focus={{
                            outline: "none",
                        }}
                    >
                        <Box
                            flex="1"
                            textAlign="left"
                            textStyle="h3"
                            color={isExpanded ? "purple.300" : "black.500"}
                            transition="all 0.5s ease-out"
                        >
                            {title}
                        </Box>
                        <Box
                            transform={isExpanded ? "rotate(45deg)" : "none"}
                            transition="all 0.5s ease-out"
                        >
                            <ArrowRight
                                color={isExpanded ? "purple.300" : "black.500"}
                            />
                        </Box>
                    </AccordionButton>
                    <AccordionPanel pb={4} textStyle="body2" color="black.500">
                        {desc}
                    </AccordionPanel>
                </Box>
            )}
        </AccordionItem>
    );
};

export default WhyWavy;
