import React, { useEffect, useRef, useState } from "react";
import PageContainer from "../../ui/pageContainer";
import PageTitle from "../../ui/pageTitle";
import ArrowsPagination from "../../ui/arrowsPagination";
import { Box, Grid, GridItem, VStack } from "@chakra-ui/react";
import Image from "next/image";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import Slider from "react-slick";

type Review = {
    title: string;
    desc: string;
    img: string;
    author: {
        name: string;
        title: string;
    };
};

const reviews: Review[] = [
    {
        title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        img: "/assets/images/reviewPhoto.png",
        author: {
            name: "Your mother boyfriend",
            title: "Managing Director of Your Mama",
        },
    },
    {
        title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        img: "/assets/images/reviewPhoto.png",
        author: {
            name: "Your mother boyfriend",
            title: "Managing Director of Your Mama",
        },
    },
    {
        title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        img: "/assets/images/reviewPhoto.png",
        author: {
            name: "Your mother boyfriend",
            title: "Managing Director of Your Mama",
        },
    },
    {
        title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        img: "/assets/images/reviewPhoto.png",
        author: {
            name: "Your mother boyfriend",
            title: "Managing Director of Your Mama",
        },
    },
    {
        title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        img: "/assets/images/reviewPhoto.png",
        author: {
            name: "Your mother boyfriend",
            title: "Managing Director of Your Mama",
        },
    },
    {
        title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        img: "/assets/images/reviewPhoto.png",
        author: {
            name: "Your mother boyfriend",
            title: "Managing Director of Your Mama",
        },
    },
    {
        title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        img: "/assets/images/reviewPhoto.png",
        author: {
            name: "Your mother boyfriend",
            title: "Managing Director of Your Mama",
        },
    },
    {
        title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        img: "/assets/images/reviewPhoto.png",
        author: {
            name: "Your mother boyfriend",
            title: "Managing Director of Your Mama",
        },
    },
    {
        title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod",
        desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
        img: "/assets/images/reviewPhoto.png",
        author: {
            name: "Your mother boyfriend",
            title: "Managing Director of Your Mama",
        },
    },
];

const Reviews: React.FC = () => {
    const [page, setPage] = useState(0);
    const ref = useRef<any>(null);
    // const wrapperRef = useRef<HTMLDivElement>(null);

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        beforeChange: (_current: any, next: number) => setPage(next),
    };

    // const slide = (event: any) => {
    //     console.log("slide event", event);
    //     event.preventDefault();
    //     event.deltaX > 0 ? ref.current.slickNext() : ref.current.slickPrev();
    // };

    // const throttledSlide = React.useMemo(() => throttle(slide, 300), []);

    // useEffect(() => {
    //     if (!wrapperRef?.current) return;
    //     wrapperRef.current.addEventListener("wheel", throttledSlide);

    //     return () =>
    //         wrapperRef?.current?.removeEventListener("wheel", throttledSlide);
    // }, []);

    return (
        <PageContainer containerPt={0}>
            <PageTitle
                title="They talk about us"
                rightComponent={
                    <ArrowsPagination
                        page={page}
                        onChange={(value) => ref.current.slickGoTo(value)}
                        length={reviews.length}
                    />
                }
            />
            {/* <div ref={wrapperRef}> */}
            <Slider ref={(slider: any) => (ref.current = slider)} {...settings}>
                {reviews.map((review) => (
                    <ReviewsItems review={review} key={review.title} />
                ))}
            </Slider>
            {/* </div> */}
        </PageContainer>
    );
};

type ReviewProps = {
    review: Review;
};

const ReviewsItems: React.FC<ReviewProps> = ({ review }) => {
    return (
        <Grid templateColumns="repeat(4, 1fr)" pt={{ base: 10, md: 24 }}>
            <GridItem
                colSpan={{ base: 4, md: 2 }}
                display="flex"
                justifyContent={{
                    base: "center",
                    md: "flex-end",
                }}
                alignItems="flex-start"
            >
                <Box
                    width={{ base: 160, md: 250 }}
                    height={{ base: 160, md: 250 }}
                    position="relative"
                    mr={{ base: 0, md: 8 }}
                    mb={{ base: 4, md: 0 }}
                >
                    <Image
                        src={review.img}
                        objectFit="cover"
                        quality={100}
                        layout="fill"
                    />
                </Box>
            </GridItem>
            <GridItem colSpan={{ base: 4, md: 2 }}>
                <VStack spacing={5} color="black.500" align="left">
                    <Box textStyle="h4">{review.title}</Box>
                    <Box textStyle="body2">{review.desc}</Box>
                    <Box textStyle="body1" textAlign="left">
                        {review.author.name}
                        <Box textStyle="caption" color="gray.500" pt={1}>
                            {review.author.title}
                        </Box>
                    </Box>
                </VStack>
            </GridItem>
        </Grid>
    );
};

export default Reviews;
