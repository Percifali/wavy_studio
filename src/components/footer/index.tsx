import React, { CSSProperties } from "react";
import { Flex, Center, Box, Container, HStack } from "@chakra-ui/react";
import Image from "next/image";
import { Logo } from "@components/logo";
import SvgEmail from "@components/icons/Email";
import { Viber } from "@components/icons";
import SvgWhatsapp from "@components/icons/Whatsapp";
import SvgViber from "@components/icons/Viber";
import SvgTelegram from "@components/icons/Telegram";

export const footerInfo = {
    email: "hello@wavy.com",
    ephone: "+373 (60) 100 333",
    telegram: "t.me/wavystudio",
    bottomInfo: {
        facebook: "...",
        instagram: "...",
        termsOfUse: "...",
        privacy: "...",
        rights: "2021-2022 Wavy Studio. All Rights Reserved",
    },
};

const footerItemSx = {
    textStyle: "h4",
    alignItems: "center",
    px: {
        base: 0,
        md: 8,
    },
};

export const Footer: React.FC = () => {
    return (
        <Flex flexDirection="column" w="100%">
            <Box
                w="100%"
                h={120}
                position="relative"
                backgroundImage="url('/assets/images/cosmos.png')"
                backgroundSize="cover"
                backgroundRepeat="no-repeat"
                backgroundPosition="center"
                display="flex"
                alignItems="center"
                justifyContent="center"
                textStyle="h2"
                color="white"
            >
                Let's get started?
            </Box>
            <Container maxW="container.xl" py={8}>
                <Flex flexDirection="column" color="white">
                    <Flex
                        alignItems="center"
                        justifyContent="space-between"
                        mb={24}
                    >
                        <Flex alignItems="center" justifyContent="flex-start">
                            <Logo />
                            <Box
                                sx={{
                                    fontFamily: "Bebas Neue",
                                    fontSize: "24px",
                                    lineHeight: "30px",
                                    letterSpacing: "0.24em",
                                    ml: 4,
                                }}
                            >
                                Wavy
                            </Box>
                        </Flex>
                        <Flex alignItems="center" justifyContent="flex-end">
                            <Flex sx={footerItemSx}>
                                <Box px={2}>
                                    <SvgEmail />
                                </Box>
                                {footerInfo.email}
                            </Flex>
                            <Flex sx={footerItemSx} textDecoration="underline">
                                <Box px={2}>
                                    <SvgTelegram />
                                </Box>
                                {footerInfo.telegram}
                            </Flex>
                            <Flex pr="0 !important" sx={footerItemSx}>
                                <Box px={2}>
                                    <SvgWhatsapp />
                                </Box>
                                {footerInfo.ephone}
                            </Flex>
                        </Flex>
                    </Flex>
                    <Flex alignItems="center" justifyContent="space-between">
                        <Flex alignItems="center">
                            <Flex alignItems="center" textStyle="body1" pr={6}>
                                Facebook
                            </Flex>
                            <Flex alignItems="center" textStyle="body1">
                                Instagram
                            </Flex>
                        </Flex>
                        <HStack
                            spacing={6}
                            textStyle="caption"
                            color="gray.500"
                        >
                            <Box>Terms of use</Box>
                            <Box>Privacy</Box>
                            <Box>{footerInfo.bottomInfo.rights}</Box>
                        </HStack>
                    </Flex>
                </Flex>
            </Container>
        </Flex>
    );
};
