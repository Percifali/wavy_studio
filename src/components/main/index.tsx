import React from "react";
import { Box, Container, Flex, Show, VStack } from "@chakra-ui/react";
import Image from "next/image";

import { Button, Header } from "@components";
import { ArrowRight } from "../icons";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import ReactTypingEffect from "react-typing-effect";
import { useRouter } from "next/router";

export const Main: React.FC = () => {
    const router = useRouter();
    return (
        <Container maxW="container.xl" h="100vh">
            <Flex
                height="100%"
                color="white.500"
                alignItems="flex-start"
                direction="column"
                position="relative"
                justifyContent="center"
            >
                <VStack gap={12} align="left" zIndex={10000}>
                    <Box textStyle={{ base: "h2", md: "h1" }}>
                        World-Class
                        <br />
                        Digital Studio
                    </Box>
                    <Box textStyle="body1">
                        We do{" "}
                        <ReactTypingEffect
                            typingDelay={250}
                            eraseDelay={2000}
                            speed={80}
                            eraseSpeed={50}
                            text={["design.", "programming.", "promotion."]}
                        />
                    </Box>
                    <Button
                        variant="white"
                        rightIcon={<ArrowRight />}
                        onClick={() => router.push("/contact")}
                    >
                        Start project
                    </Button>
                </VStack>
                <Show above="md">
                    <Box
                        position="absolute"
                        right={0}
                        top="18%"
                        width="55%"
                        height="55%"
                    >
                        <Image
                            src="/assets/images/hero.png"
                            objectFit="contain"
                            layout="responsive"
                            width="100%"
                            height="100%"
                            quality={100}
                        />
                    </Box>
                </Show>
            </Flex>
        </Container>
    );
};
