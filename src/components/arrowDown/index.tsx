import { Box } from "@chakra-ui/react";
import React from "react";
import { ArrowRight } from "../icons";

type Props = {
    onClick: () => void;
};

const ArrowDown: React.FC<Props> = ({ onClick }) => {
    return (
        <Box
            sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                position: "absolute",
                bottom: 12,
                right: "50%",
                color: "white",
                transform: "rotate(90deg) translate(0, -50%)",
                border: "1px solid white",
                borderRadius: "50%",
                p: 2,
                cursor: "pointer",
                transition: "all 0.25s ease-out",
                "&:hover": {
                    color: "purple.300",
                    border: "1px solid",
                    borderColor: "purple.300",
                },
            }}
            onClick={onClick}
        >
            <ArrowRight />
        </Box>
    );
};

export default ArrowDown;
