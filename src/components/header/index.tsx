import React from "react";
import {
    Box,
    BoxProps,
    Button,
    Center,
    Container,
    Drawer,
    DrawerBody,
    DrawerCloseButton,
    DrawerContent,
    DrawerFooter,
    DrawerHeader,
    DrawerOverlay,
    Flex,
    HStack,
    Text,
    useDisclosure,
    useMediaQuery,
} from "@chakra-ui/react";

import { Logo } from "@components";
import SvgHamburger from "@components/icons/Hamburger";
import SvgClose from "@components/icons/Close";
import SvgEmail from "@components/icons/Email";
import SvgViber from "@components/icons/Viber";
import SvgWhatsapp from "@components/icons/Whatsapp";
import SvgTelegram from "@components/icons/Telegram";
import { footerInfo } from "@components/footer";
import useHeader from "../../utils/useHeader";
import Link from "next/link";
import { useRouter } from "next/router";

type Props = BoxProps;

export const Header: React.FC<Props> = ({ ...props }) => {
    const { white, shrunk } = useHeader();
    const [isMobile] = useMediaQuery("(max-width: 48rem)");
    const { isOpen, onOpen, onClose } = useDisclosure();
    const router = useRouter();
    const btnRef = React.useRef(null);

    const goTo = (url: string) => {
        onClose();
        router.push(url);
    };

    return (
        <Container
            maxW="container.xl"
            py={shrunk ? 0 : 8}
            display="flex"
            alignSelf="center"
            position="absolute"
            zIndex="1000"
            backgroundColor={white ? "#FFFFFF" : "transparent"}
            borderBottom={`2px solid ${white ? "#111111" : "#FFFFFF"}`}
            color={white ? "#111111" : "#FFFFFF"}
            sx={{
                "& svg:first-child": {
                    width: shrunk ? 0 : "60px",
                    color: white ? "#111111" : "#FFFFFF",
                    transition: "all 0.5s ease-out",
                },
                "& svg:last-child": {
                    color: white ? "#111111" : "#FFFFFF",
                    width: shrunk ? "1.5em" : "2em",
                    height: shrunk ? "1.5em" : "2em",
                    transition: "all 0.5s ease-out",
                },
            }}
            transition={`all 0.5s ease-out, background-color ${
                white ? "1s" : "0s"
            } ease-out ${white ? "0.5s" : "0s"}`}
            alignItems="center"
            justifyContent="space-between"
            w="100%"
            {...props}
        >
            <Link href="/">
                <Flex
                    alignItems="center"
                    justifyContent="flex-start"
                    cursor="pointer"
                >
                    <Logo />
                    <Box
                        sx={{
                            fontFamily: "Bebas Neue",
                            fontSize: "24px",
                            lineHeight: "30px",
                            letterSpacing: "0.24em",
                            transition: "all 0.5s ease-out",
                            ml: shrunk ? 0 : 4,
                        }}
                    >
                        Wavy
                    </Box>
                </Flex>
            </Link>
            {isMobile ? (
                <Box ref={btnRef} onClick={onOpen} cursor="pointer">
                    <SvgHamburger width="2em" height="2em" />
                    <Drawer
                        isOpen={isOpen}
                        placement="right"
                        onClose={onClose}
                        finalFocusRef={btnRef}
                        size="full"
                    >
                        <DrawerOverlay />
                        <DrawerContent w="100%">
                            <Flex
                                py={8}
                                pl={4}
                                pr={4}
                                alignItems="center"
                                justifyContent="space-between"
                            >
                                <Flex
                                    alignItems="center"
                                    justifyContent="flex-start"
                                    onClick={() => goTo("/")}
                                >
                                    <Logo />
                                    <Box
                                        sx={{
                                            fontFamily: "Bebas Neue",
                                            fontSize: "24px",
                                            lineHeight: "30px",
                                            letterSpacing: "0.24em",
                                            transition: "all 0.5s ease-out",
                                            ml: 4,
                                        }}
                                    >
                                        Wavy
                                    </Box>
                                </Flex>
                                <Box cursor="pointer" onClick={onClose}>
                                    <SvgClose
                                        color="black"
                                        width="1.6em"
                                        height="1.6em"
                                    />
                                </Box>
                            </Flex>
                            <DrawerBody
                                display="flex"
                                flexDirection="column"
                                justifyContent="flex-start"
                                alignItems="center"
                                color="black.500"
                                pt={20}
                            >
                                <Box
                                    textStyle="h4"
                                    py={4}
                                    width={250}
                                    textAlign="center"
                                    borderBottom="2px solid"
                                    onClick={() => goTo("/")}
                                    borderBottomColor={
                                        router.asPath === "/"
                                            ? "purple.300"
                                            : "black.500"
                                    }
                                    color={
                                        router.asPath === "/"
                                            ? "purple.300"
                                            : "black.500"
                                    }
                                >
                                    Home
                                </Box>

                                <Box
                                    textStyle="h4"
                                    py={4}
                                    width={250}
                                    textAlign="center"
                                    borderBottom="2px solid"
                                    onClick={() => goTo("/clients")}
                                    borderBottomColor={
                                        router.asPath === "/clients"
                                            ? "purple.300"
                                            : "black.500"
                                    }
                                    color={
                                        router.asPath === "/clients"
                                            ? "purple.300"
                                            : "black.500"
                                    }
                                >
                                    Clients
                                </Box>

                                <Box
                                    textStyle="h4"
                                    py={4}
                                    width={250}
                                    textAlign="center"
                                    borderBottom="2px solid"
                                    onClick={() => goTo("/contact")}
                                    borderBottomColor={
                                        router.asPath === "/contact"
                                            ? "purple.300"
                                            : "black.500"
                                    }
                                    color={
                                        router.asPath === "/contact"
                                            ? "purple.300"
                                            : "black.500"
                                    }
                                >
                                    Contact
                                </Box>

                                <Flex
                                    mt="auto"
                                    direction="column"
                                    justifyContent="center"
                                    alignItems="center"
                                    pr={12}
                                    sx={{
                                        path: {
                                            fill: "black.500",
                                        },
                                    }}
                                >
                                    <Flex textStyle="h4">
                                        <Flex
                                            px={2}
                                            w={20}
                                            alignItems="center"
                                            justifyContent="flex-end"
                                        >
                                            <SvgEmail />
                                        </Flex>
                                        {footerInfo.email}
                                    </Flex>
                                    <Flex textStyle="h4" pt={2}>
                                        <Flex
                                            px={2}
                                            w={20}
                                            alignItems="center"
                                            justifyContent="flex-end"
                                        >
                                            <Box>
                                                <SvgViber />
                                            </Box>
                                            <Box pl={2}>
                                                <SvgWhatsapp />
                                            </Box>
                                        </Flex>
                                        {footerInfo.ephone}
                                    </Flex>
                                    <Flex
                                        textStyle="h4"
                                        textDecoration="underline"
                                        pt={2}
                                    >
                                        <Flex
                                            px={2}
                                            w={20}
                                            alignItems="center"
                                            justifyContent="flex-end"
                                        >
                                            <SvgTelegram />
                                        </Flex>
                                        {footerInfo.telegram}
                                    </Flex>
                                </Flex>
                            </DrawerBody>
                        </DrawerContent>
                    </Drawer>
                </Box>
            ) : (
                <HStack spacing={6} textStyle="body1">
                    <Link href="/clients">
                        <Box
                            cursor="pointer"
                            sx={{
                                transition: "all 0.2s ease-in-out",
                                color:
                                    router.asPath === "/clients"
                                        ? "purple.300"
                                        : white
                                        ? "black.500"
                                        : "white",
                                "&:hover": {
                                    color: "purple.300",
                                },
                            }}
                        >
                            Clients
                        </Box>
                    </Link>
                    {/*<Link href="/work">*/}
                    {/*    <Box*/}
                    {/*        cursor="pointer"*/}
                    {/*        sx={{*/}
                    {/*            transition: "all 0.2s ease-in-out",*/}
                    {/*            color:*/}
                    {/*                router.asPath === "/work"*/}
                    {/*                    ? "purple.300"*/}
                    {/*                    : white*/}
                    {/*                    ? "black.500"*/}
                    {/*                    : "white",*/}
                    {/*            "&:hover": {*/}
                    {/*                color: "purple.300",*/}
                    {/*            },*/}
                    {/*        }}*/}
                    {/*    >*/}
                    {/*        Our work*/}
                    {/*    </Box>*/}
                    {/*</Link>*/}
                    <Link href="/contact">
                        <Box
                            cursor="pointer"
                            sx={{
                                transition: "all 0.2s ease-in-out",
                                color:
                                    router.asPath === "/contact"
                                        ? "purple.300"
                                        : white
                                        ? "black.500"
                                        : "white",
                                "&:hover": {
                                    color: "purple.300",
                                },
                            }}
                        >
                            Contact us
                        </Box>
                    </Link>
                </HStack>
            )}
        </Container>
    );
};
