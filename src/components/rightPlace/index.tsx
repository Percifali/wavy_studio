import React from "react";
import { Box, Button } from "@chakra-ui/react";
import { ArrowRight } from "@components/icons";
import { useRouter } from "next/router";

const RightPlace: React.FC = () => {
    const router = useRouter();
    return (
        <Box
            h={300}
            display="flex"
            flexDirection="column"
            alignItems="center"
            justifyContent="center"
            color="white.500"
        >
            <Box textStyle="h2" textAlign="center" mb={12}>
                You’ve come to the right place
            </Box>
            <Button
                variant="white"
                rightIcon={<ArrowRight />}
                onClick={() => router.push("/contact")}
            >
                Schedule a consultation
            </Button>
        </Box>
    );
};

export default RightPlace;
