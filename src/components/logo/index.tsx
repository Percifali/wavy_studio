import React from "react";

import SvgWavy from "@components/icons/Wavy";

export const Logo: React.FC = () => {
    return <SvgWavy width="60" height="60" />;
};
