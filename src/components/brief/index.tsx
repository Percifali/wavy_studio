import React from "react";
import {
    Box,
    Button,
    Flex,
    Grid,
    GridItem,
    Hide,
    Input,
    Textarea,
} from "@chakra-ui/react";
import { ArrowRight } from "@components/icons";
import { Footer } from "@components";

const Brief: React.FC = () => {
    return (
        <Flex
            flexDirection="column"
            justifyContent="flex-start"
            alignItems="center"
            height="100vh"
        >
            <BriefForm />
            <Hide below="md">
                <Footer />
            </Hide>
        </Flex>
    );
};

export const BriefForm = () => {
    return (
        <Flex
            flexDirection="column"
            alignItems="center"
            justifyContent={{
                base: "center",
                md: "end",
            }}
            color="white"
            pb={{
                base: 0,
                md: 24,
            }}
            h="100%"
        >
            <Box textStyle="h2">Fill out the brief</Box>
            <Grid templateColumns="repeat(4, 1fr)" gap={4} my={4}>
                <GridItem colSpan={{ base: 4, md: 2 }}>
                    <Input
                        placeholder="Name"
                        size="lg"
                        borderRadius={0}
                        focusBorderColor="purple.300"
                    />
                </GridItem>
                <GridItem colSpan={{ base: 4, md: 2 }}>
                    <Input
                        placeholder="Phone number"
                        size="lg"
                        borderRadius={0}
                        focusBorderColor="purple.300"
                    />
                </GridItem>
                <GridItem colSpan={{ base: 4, md: 2 }}>
                    <Input
                        placeholder="Email"
                        size="lg"
                        borderRadius={0}
                        focusBorderColor="purple.300"
                    />
                </GridItem>
                <GridItem colSpan={{ base: 4, md: 2 }}>
                    <Input
                        placeholder="What are you looking for?"
                        size="lg"
                        borderRadius={0}
                        focusBorderColor="purple.300"
                    />
                </GridItem>
                <GridItem colSpan={4}>
                    <Textarea
                        placeholder="What are you looking for?"
                        size="lg"
                        borderRadius={0}
                        minHeight={150}
                        maxHeight={300}
                        focusBorderColor="purple.300"
                    />
                </GridItem>
            </Grid>
            <Flex>
                <Button variant="white" rightIcon={<ArrowRight />}>
                    Submit
                </Button>
            </Flex>
        </Flex>
    );
};

export default Brief;
