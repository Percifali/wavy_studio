import React, {
    createContext,
    ReactNode,
    useContext,
    useEffect,
    useMemo,
} from "react";
import { useRouter } from "next/router";

interface HeaderProviderType {
    shrunk: boolean;
    white: boolean;
    setShrunk: (value: boolean) => void;
    setWhite: (value: boolean) => void;
}

const initialData: HeaderProviderType = {
    shrunk: false,
    white: false,
    setShrunk: () => null,
    setWhite: () => null,
};

const HeaderContext = createContext<HeaderProviderType>(initialData);

export function HeaderProvider({
    children,
}: {
    children: ReactNode;
}): JSX.Element {
    const [shrunk, setShrunk] = React.useState(false);
    const [white, setWhite] = React.useState(false);
    const router = useRouter();

    useEffect(() => {
        setShrunk(false);
        setWhite(false);
    }, [router.asPath]);

    const memoedValue = useMemo(
        () => ({
            shrunk,
            white,
            setShrunk,
            setWhite,
        }),
        [shrunk, white],
    );

    return (
        <HeaderContext.Provider value={memoedValue}>
            {children}
        </HeaderContext.Provider>
    );
}

export default function useHeader() {
    return useContext(HeaderContext);
}
