import type { ComponentStyleConfig } from "@chakra-ui/theme";

// You can also use the more specific type for
// a single part component: ComponentSingleStyleConfig
const Button: ComponentStyleConfig = {
    // The styles all button have in common
    baseStyle: {
        fontWeight: "regular",
        cursor: "pointer",
        border: "none",
        "&:hover": {
            "& .chakra-button__icon": {
                transform: "translate(10px, 0)",
            },
        },
        "& .chakra-button__icon": {
            transition: "all .2s ease-out",
        },
    },
    // Two sizes: sm and md
    sizes: {
        icon: { px: 2, py: 0 },
        sm: {
            fontSize: "16px",
            lineHeight: "20px",
            letterSpacing: "1%",
            fontWeight: "500",
            borderRadius: "15px",
            height: "30px",
            px: 3,
        },
        md: {
            fontSize: "18px",
            lineHeight: "18px",
            letterSpacing: "0%",
            fontWeight: "500",
            height: "45px",
            px: 6,
            borderRadius: "25px",
        },
    },
    // Two variants: outline and solid
    variants: {
        outline: {
            border: "2px solid",
            borderColor: "blue.400",
            color: "blue.400",
        },
        solid: {
            bg: "blue.400",
            color: "white",
        },
        black: {
            color: "white.500",
            backgroundColor: "black.500",
            border: "2px solid",
            borderColor: "black.500",
            transition: "all .2s ease-in-out 0s",
            "&:hover": {
                backgroundColor: "transparent",
                color: "purple.300",
                border: "2px solid",
                borderColor: "purple.300",
            },
        },
        white: {
            color: "black.500",
            backgroundColor: "white.500",
            border: "2px solid",
            borderColor: "white.500",
            transition: "all .2s ease-in-out 0s",
            "&:hover": {
                backgroundColor: "transparent",
                color: "purple.300",
                border: "2px solid",
                borderColor: "purple.300",
            },
        },
    },
    // The default size and variant values
    defaultProps: {
        size: "md",
        variant: "outline",
    },
};

export default Button;
