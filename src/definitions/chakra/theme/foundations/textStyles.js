export default {
    h1: {
        fontSize: "5rem",
        fontWeight: "600",
        lineHeight: "6rem",
        letterSpacing: "8%",
    },
    h2: {
        fontSize: "3rem",
        fontWeight: "600",
        lineHeight: "110%",
        letterSpacing: "8%",
    },
    h3: {
        fontSize: "2rem",
        fontWeight: "500",
        lineHeight: "110%",
        letterSpacing: "4%",
    },
    h4: {
        fontSize: "1.5rem",
        fontWeight: "500",
        lineHeight: "110%",
        letterSpacing: "4%",
    },
    h5: {
        fontSize: "1.25rem",
        fontWeight: "500",
        lineHeight: "120%",
        letterSpacing: "0%",
    },
    body1: {
        fontSize: "1.125rem",
        fontWeight: "500",
        lineHeight: "110%",
        letterSpacing: "4%",
    },
    body2: {
        fontSize: "1.125rem",
        fontWeight: "400",
        lineHeight: "110%",
        letterSpacing: "0%",
    },
    blueLink: {
        fontSize: "1rem",
        fontWeight: "500",
        lineHeight: "120%",
        letterSpacing: "2%",
        cursor: "pointer",
        color: "blue.400",
        textDecoration: "underline",
        "&:hover": {
            color: "blue.500",
        },
    },
    link: {
        fontSize: "1rem",
        fontWeight: "500",
        lineHeight: "120%",
        letterSpacing: "2%",
        cursor: "pointer",
        color: "black.800",
        "&:hover": {
            textDecoration: "underline",
        },
    },
    button: {
        fontSize: "1rem",
        fontWeight: "700",
        lineHeight: "120%",
        letterSpacing: "2%",
    },
    caption: {
        fontSize: "0.75rem",
        fontWeight: "400",
        lineHeight: "120%",
        letterSpacing: "0%",
    },
};
