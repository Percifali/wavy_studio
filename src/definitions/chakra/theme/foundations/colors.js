export default {
    brand: {
        500: "#3045C0",
    },
    black: {
        500: "#111111",
    },
    white: {
        500: "#FFFFFF",
    },
    gray: {
        500: "#898989",
    },
};
