import React from "react";
import { Box, Button, Flex } from "@chakra-ui/react";
import { ArrowRight } from "@components/icons";

type Props = {
    onChange: (value: number) => void;
    page: number;
    length: number;
};

const buttonSx = {
    display: "flex",
    color: "black.500",
    borderColor: "black.500",
    p: 0,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: "50%",
    border: "2px solid #111111",
    height: "40px",
    width: "40px",
};

const ArrowsPagination: React.FC<Props> = ({ onChange, page, length }) => {
    const next = () => {
        if (page + 1 === length) {
            onChange(0);
            return;
        }
        onChange(page + 1);
    };

    const prev = () => {
        if (page === 0) {
            onChange(length - 1);
            return;
        }
        onChange(page - 1);
    };

    return (
        <Flex alignItems="center" justifyContent="flex-end">
            <Button sx={buttonSx} transform="rotate(180deg)" onClick={prev}>
                <ArrowRight />
            </Button>
            <Flex
                alignItems="center"
                justifyContent="center"
                textStyle="h4"
                fontWeight="600"
                letterSpacing="8%"
                width={24}
                px={4}
            >
                {page + 1}
                <Box color="gray.500">&nbsp;/&nbsp;{length}</Box>
            </Flex>
            <Button sx={buttonSx} onClick={next}>
                <ArrowRight />
            </Button>
        </Flex>
    );
};

export default ArrowsPagination;
