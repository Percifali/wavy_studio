import React from "react";
import { Box, BoxProps } from "@chakra-ui/react";

type Props = {
    title: string;
} & BoxProps;

const SimpleTitle: React.FC<Props> = ({ title, ...props }) => {
    return (
        <Box textStyle={{ base: "h3", md: "h2" }} color="black.500" {...props}>
            {title}
        </Box>
    );
};

export default SimpleTitle;
