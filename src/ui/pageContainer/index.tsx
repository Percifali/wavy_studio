import { BoxProps, Container, Flex } from "@chakra-ui/react";
import React from "react";

type Props = BoxProps & {
    containerPt?: number;
};

const PageContainer: React.FC<Props> = ({
    containerPt = 20,
    children,
    ...props
}) => {
    return (
        <Flex
            height="100vh"
            width="100%"
            alignItems="center"
            justifyContent="center"
            backgroundColor="white.500"
            {...props}
        >
            <Container maxW="container.xl" pt={containerPt}>
                {children}
            </Container>
        </Flex>
    );
};

export default PageContainer;
