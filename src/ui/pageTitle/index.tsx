import React from "react";
import { BoxProps, Flex } from "@chakra-ui/react";
import SimpleTitle from "../simpleTitle";

type Props = {
    title: string;
    rightComponent?: JSX.Element;
} & BoxProps;

const PageTitle: React.FC<Props> = ({ title, rightComponent, ...props }) => {
    return (
        <Flex
            alignItems="center"
            justifyContent="space-between"
            color="black.500"
            {...props}
        >
            <SimpleTitle title={title} />
            {rightComponent}
        </Flex>
    );
};

export default PageTitle;
