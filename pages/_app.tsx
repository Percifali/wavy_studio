import React from "react";
import { AppProps } from "next/app";
import { Box, ChakraProvider } from "@chakra-ui/react";
import theme from "@definitions/chakra/theme";
import "@styles/global.css";
import { HeaderProvider } from "../src/utils/useHeader";
import { Footer, Header } from "@components";

function MyApp({ Component, pageProps }: AppProps): JSX.Element {
    return (
        <ChakraProvider theme={theme}>
            <HeaderProvider>
                <Box
                    sx={{
                        "& .fp-watermark": {
                            display: "none",
                        },
                        display: "flex",
                        flexDirection: "column",
                        justifyContent: "center",
                        alignItems: "stretch",
                        overflow: "hidden",
                        //minWidth: "fit-content",
                    }}
                >
                    <Header top={0} />
                    <Component {...pageProps} />
                </Box>
            </HeaderProvider>
        </ChakraProvider>
    );
}

export default MyApp;
