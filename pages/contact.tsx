import React from "react";
import { Footer, Header } from "@components";
import { Box, Container, Flex, Hide } from "@chakra-ui/react";
import { BriefForm } from "@components/brief";

const Contact = () => {
    return (
        <Flex
            flexDirection="column"
            justifyContent="flex-start"
            alignItems="center"
            height="100vh"
        >
            <BriefForm />
            <Hide below="md">
                <Footer />
            </Hide>
        </Flex>
    );
};

export default Contact;
