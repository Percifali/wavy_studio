import React from "react";
import { Header } from "@components";
import { Box } from "@chakra-ui/react";

const Work = () => {
    return (
        <Box
            sx={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "stretch",
            }}
        >
            <Header top={0} />
            Work
        </Box>
    );
};

export default Work;
