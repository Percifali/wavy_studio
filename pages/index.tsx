import React, { useRef } from "react";
import { Spacer, Flex, Container, Box } from "@chakra-ui/react";

import { Footer, Header, Main } from "@components";
import WhatIsWavy from "@components/whatIsWavy";
import ReactFullpage from "@fullpage/react-fullpage";
import OurProjects from "@components/ourProjects";
import WhyWavy from "@components/whyWavy";
import Reviews from "@components/reviews";
import Brief from "@components/brief";
import useHeader from "../src/utils/useHeader";
import ArrowDown from "../src/components/arrowDown";
import ArrowUp from "@components/arrowUp";

const pages = {
    "1": Main,
    "2": WhatIsWavy,
    "3": OurProjects,
    "4": WhyWavy,
    "5": Reviews,
    "6": Brief,
};

const pluginWrapper = () => {
    /*
     * require('../static/fullpage.scrollHorizontally.min.js'); // Optional. Required when using the "scrollHorizontally" extension.
     */
};

const Home: React.FC = () => {
    const { setShrunk, setWhite } = useHeader();
    const ref = useRef<null | {
        moveTo: (section: string | number, slide?: string | number) => void;
    }>();
    return (
        <Box
            sx={{
                "& .fp-watermark": {
                    display: "none",
                },
                "& .section": {
                    overflow: "hidden",
                },
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "stretch",
            }}
        >
            <ReactFullpage
                navigation
                //sectionsColor={originalColors}
                onLeave={(from, to) => {
                    setShrunk(to.index !== 0);
                    setWhite(to.index !== 0 && to.index !== 5);
                }}
                pluginWrapper={pluginWrapper}
                render={({ fullpageApi }) => {
                    if (ref.current == null) {
                        // @ts-ignore
                        ref.current = fullpageApi;
                    }
                    return (
                        <ReactFullpage.Wrapper>
                            {Object.keys(pages).map((key: any) => {
                                const Handler =
                                    pages[key as keyof typeof pages];
                                return (
                                    <div className="section" key={key}>
                                        <Handler />
                                    </div>
                                );
                            })}
                            <ArrowDown
                                onClick={() => fullpageApi.moveSectionDown()}
                            />
                        </ReactFullpage.Wrapper>
                    );
                }}
            />
            <ArrowUp onClick={() => ref?.current?.moveTo("1")} />
        </Box>
    );
};

export default Home;
