import React from "react";
import { Footer, Header } from "@components";
import { Box, Container, SimpleGrid } from "@chakra-ui/react";
import RightPlace from "@components/rightPlace";

const Clients = () => {
    return (
        <>
            <Container maxW="container.xl">
                <Box
                    mt={150}
                    h={400}
                    display="flex"
                    flexDirection="column"
                    alignItems="left"
                    justifyContent="center"
                    width={{
                        base: "90%",
                        md: "60%",
                    }}
                >
                    <Box textStyle="h2" color="white">
                        Our client relationships go beyond technology
                    </Box>
                    <Box textStyle="h4" color="white" mt={4}>
                        We created a second project for over 80% of our clients.
                        A product is ephemeral, a relationship is for life.
                    </Box>
                </Box>
                <SimpleGrid
                    columns={{
                        base: 2,
                        md: 3,
                    }}
                    spacing={12}
                >
                    <ClientBox />
                    <ClientBox />
                    <ClientBox />
                    <ClientBox />
                    <ClientBox />
                    <ClientBox />
                </SimpleGrid>
                <Box
                    h={400}
                    display="flex"
                    flexDirection="column"
                    alignItems="left"
                    justifyContent="center"
                >
                    <Box textStyle="h2" color="white">
                        <b>We don’t just build products,</b>
                    </Box>
                    <Box textStyle="h2" color="white" fontWeight={500}>
                        we build relationships
                    </Box>
                </Box>
                <SimpleGrid
                    columns={{
                        base: 1,
                        md: 3,
                    }}
                    spacing={12}
                    mb={24}
                >
                    <HeroBox />
                    <HeroBox />
                    <HeroBox />
                    <HeroBox />
                    <HeroBox />
                    <HeroBox />
                </SimpleGrid>
            </Container>
            <RightPlace />
            <Footer />
        </>
    );
};

const HeroBox = () => {
    return (
        <Box
            display="flex"
            flexDirection="column"
            alignItems="left"
            justifyContent="center"
        >
            <Box display="flex" maxWidth="85%" alignItems="center">
                <Box
                    width="70px"
                    height="70px"
                    sx={{
                        "& img": {
                            height: "100%",
                            maxWidth: "none",
                        },
                    }}
                >
                    <img src="https://compire.co/static/media/kong-pham.b8013d8c.jpg" />
                </Box>
                <Box color="white" ml={4}>
                    <Box textStyle="h4" fontWeight={600}>
                        Asian Fucker
                    </Box>
                    <Box textStyle="body1" pt={1}>
                        Director of Engineering, Luxury Presence
                    </Box>
                </Box>
            </Box>
            <Box textStyle="body2" color="white" fontSize="1rem" mt={6}>
                Our invaluable partnership with this YC company with over 20,000
                premium users resulted in an online video education platform
                with a fast, seamless and smooth user experience that inspires
                and works fast on every browser.
            </Box>
        </Box>
    );
};

const ClientBox = () => {
    return (
        <Box
            display="flex"
            flexDirection="column"
            alignItems="left"
            justifyContent="center"
        >
            <Box
                sx={{
                    marginBottom: "30px",
                    minHeight: "80px",
                    display: "flex",
                    alignItems: "flex-end",
                    "& img": {
                        filter: "brightness(0) invert(1)",
                        maxWidth: "100%",
                        verticalAlign: "middle",
                    },
                }}
            >
                <img src="https://compire.co/static/media/jumpcut.e98f9253.svg" />
            </Box>
            <Box textStyle="body1" color="white">
                Our invaluable partnership with this YC company with over 20,000
                premium users resulted in an online video education platform
                with a fast, seamless and smooth user experience that inspires
                and works fast on every browser.
            </Box>
        </Box>
    );
};

export default Clients;
